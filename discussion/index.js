// console.log("hello");

// ARRAY  METHODS

/*

		Mutator Methods
				- seeks to modify the contents of an array.
				- are functions that mutates and array after they are created. These methods manipulate original array performing various tasks such as adding or removing elements.
*/

let fruits = ["Apple", "Orange", "Kiwi", "Watermelon"];

/*
		push()
		 - adds an element in the end of an array and returns the array's length

		 Syntax:
		  arrayName.push(element)
*/

console.log("Current Fruits Array:");
console.log(fruits);

//Adding element/s

fruits.push("Mango");
console.log(fruits);
let fruitsLength = fruits.push("Melon");
console.log(fruitsLength);
console.log(fruits);

fruits.push("Avocado", "Guava");
console.log(fruits);

/*
	pop()
	 - removes the last element in our array and returns the removed element (when applied inside a variable)

	 Syntax:
	   arrayName.pop()
*/

let removedFruit = fruits.pop()
console.log(removedFruit);
console.log("Mutated array from the pop method:");
console.log(fruits);

/*
	unshift()
	  - adds one or more elements at the beginning of an array
	  - returnd the lenght of the array (when presented or stored inside a variable)

	Syntax:
			arrayName.unshift(elementA)
			arrayName.unshift(elementA, elementB)
*/

fruits.unshift("Lime", "Banana");
console.log("Mutated Array from the unshift method:");
console.log(fruits);

/*
	shift()
	  - removes an element at the beginning of our array and returns the removed element

	Syntax:
			arrayName.shift() 
*/

let removedFruit2 = fruits.shift();
console.log("removedFruit2");
console.log("Mutated array from the shift method:")
console.log(fruits);

/*
		splice()
		  - allows to simultaneously remove elements from a specified index number and adds an element 

		Syntax:
				arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)
*/

let fruitsSplice = fruits.splice(1, 2, "Cherry", "Lychee");
console.log("Mutated arrays from the splice method:");
console.log(fruits);
console.log(fruitsSplice);

// Using splice but without adding elements
let removedSplice = fruits.splice(3, 2);
console.log(fruits);
console.log(removedSplice);

/*
	sort()
	  - rearannges the array elements in alphanumberic order

	Syntax:
			arrayName.sort()
*/

// fruits.sort()
console.log("Mutated array from sort method:");
// console.log(fruits);

let mixedArr = [12, "May", 36, 94, "August", 5, 6.3, "September", 10, 100, 1000];
console.log(mixedArr.sort());

/*
	reverse()
	  - reverses the order of the elements in an array

	Syntax:
			arrayName.reverse();
*/

fruits.reverse()
console.log("Mutated array from reverse method:");
console.log(fruits);

// For sorting the items in descending order:

fruits.sort().reverse()
console.log(fruits);

// Mini-Activity
 	 // - Debug the function which will allow us to list fruits in the fruits array.
	 	// -- this function should be able to receive a string.
	 	// -- determine if the input fruit name already exist in the fruits array.
	 	// 	*** If it does, show an alert message: "Fruit already listed on our inventory".
	 	// 	*** If not, add the new fruit into the fruits array ans show an alert message: "Fruit is not listed in our inventory."
	 	// -- invoke and register a new fruit in the fruit array.
	 	// -- log the updated fruits array in the console


	 	function registerFruit (fruitName) {
	 		let doesFruitExist = fruits.includes(fruitName);

	 		if(doesFruitExist) {
	 			alert(fruitName + " is already on our inventory")
	 		} else {
	 			fruits.push(fruitName);		
	 			alert("Fruit is now listed in our inventory")
	 		}
	 	}
	 	
	 	registerFruit("Papaya");
	 	console.log(fruits);

// Mini Activity - Array Mutators:


let citiesInTexas =[];
citiesInTexas.push("Houston", "Anna", "Pearland", "Carrollton");
console.log(citiesInTexas);

citiesInTexas.unshift("Grand Prairie", "Sugar Land")
console.log(citiesInTexas);

citiesInTexas.pop();
console.log(citiesInTexas);

citiesInTexas.shift();
console.log(citiesInTexas);

citiesInTexas.sort();
console.log(citiesInTexas);
		
/*
		Non-Mutator Methods
				- these are funcstions or methods that do not modify or change an array after they are createrd.
				- these methods do not maniulate the original array performing various tasts such as returning elements from an array and combining arrays and printing the output.
*/

let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];
console.log(countries);

/*
	indexOf()
		- returns the index number of the first matching element found in an array. If no match was found, the result will be -1. The search process will  be done from our first element proceeding to the last element

	Syntax:
		arrayName.indexOf(searchValue);
		arrayName.indexOf(searchValue, fromIndex);
*/

let firstIndex = countries.indexOf("PH");
console.log("Result of indexOf method: " + firstIndex);

firstIndex = countries.indexOf("PH", 4);
console.log("Result of indexOf method: " + firstIndex);
firstIndex = countries.indexOf("PH", 7);
console.log("Result of indexOf method: " + firstIndex);
firstIndex = countries.indexOf("PH", -1);
console.log("Result of indexOf method: " + firstIndex);

console.log("");

/*
	lastIndexOf()
		- returns the index number of the last matching element found in an array. The search process will be done from the last element proceedin to the first.

	Syntax:
		arrayName.lastIndexOf(searchValue);
		arrayName.lastIndexOf(searchValue, fromIndex);
*/

let lastIndex = countries.lastIndexOf("PH");
console.log("Result of lastIndexOf method: " + lastIndex);
lastIndex = countries.lastIndexOf("PH", 4);
console.log("Result of lastIndexOf method: " + lastIndex);

/*
	slice()
	  - portions / slices elements from our array and returns a new array

	Syntax:
		arrayName.slice(startingIndex)
		arrayName.slice(startingIndex, endingIndex)
*/

console.log(countries);

let slicedArrayA = countries.slice(2)
console.log("Result from slice method: ");
console.log(slicedArrayA);
console.log(countries);

console.log("");

let slicedArrayB = countries.slice(2, 5);
console.log("Result from slice method: ");
console.log(slicedArrayB);
console.log(countries);

console.log("");


let slicedArrayC = countries.slice(-3)
console.log("Result from slice method: ");
console.log(slicedArrayC);
console.log(countries);


console.log("");

/*
	toString()
		- returns an array as a string separated by commas
		- is used internally by JS when an array needs to be displayed as a text(like in HTML), or when an object/array needs to be used as a string.
	
	Syntax:
		arrayName.toString()
*/

let stringArray = countries.toString()
console.log("Result from toString method:");
console.log(stringArray);


let mixedArrToString = mixedArr.toString()
console.log("Result from toString method:");
console.log(mixedArrToString);

console.log("");

/*
	concat()
		- combnes 2 or more arrays and returns the combined result

	Syntax:
		arrayA.concat(arrayB)
		arrayA.concat(elementA)
*/

let taskArrayA = ["drink HTML", "eat Javascript"];
let taskArrayB = ["inhale CSS", "breathe SASS"];
let taskArrayC = ["get git", "be node"];

let tasks = taskArrayA.concat(taskArrayB);
console.log("Result from concat method:");
console.log(tasks);

let allTasks = taskArrayA.concat(taskArrayB, taskArrayC);
console.log(allTasks);

// Combining arrays with element - similar to push
let combineTask = taskArrayA.concat("smell express", "throw react");
console.log(combineTask);

/*
	join()
		- returns an array as a string
		- does not change the original array
		- we can use any type of separator. The default is comma (,)
	
	Syntax:
		arrayName.join(separatorString)
*/
console.log("");

let students = ["Elysha", "Gab", "Ronel", "Jean"];
console.log(students.join());
console.log(students.join(' '));
console.log(students.join(" - "));


console.log("");
/*
	Iteration Methods
		- are loops designed to perform repetitive tasks in an array. Used for manipulating array data resulting in complex tasks.
		- normally worked with a function supplied as an argument.
		- aims to evaluate each element in an array
*/

/*
	forEach()
	 - similar to for loop that iterates on each array element


	Syntax:
		arrayName.forEach(function(individualElement) {
			statement
		});
*/

allTasks.forEach(function(task) {
	console.log(task)
});

console.log("");

// Using forEach with conditional statements

let filteredTasks = []

allTasks.forEach(function(task) {
	console.log(task)
	if(task.length > 10) {
		filteredTasks.push(task)
	}
});

console.log(allTasks);
console.log("Result of filteredTasks: ");
console.log(filteredTasks);


console.log("");

/*
	map()
		- iterates on each element and returns a new array with different values depending on the result of the function's operation

	Syntax:
		arrayName.map(function(individualElement) {
			statement
		})
*/

let numbers = [1, 2, 3, 4, 5];
let numberMap = numbers.map(function(number) {
	console.log(number);
	return number*number
})

console.log("Original Array");
console.log(numbers);
console.log("Result of the map method:");
console.log(numberMap);

/*
	every()
	 - checks if all elements in an array met the given condition. Returns a true value if all elements meet the condition and false if otherwise

	Syntax:
		arrayName.every(function(individualElement) {
			return expression/condition 
		})
*/

let allValid = numbers.every(function(number) {
	return (number < 3);
})

console.log("Result of every method: ");
console.log(allValid);

/*
	some()
	 - checks iff at least one leement in the array meet the given condition. Returns a true value if at least one of the elements meets the given condition and false if otherwise.

	Syntax:
		arrayName.some(function(individualElement) {
			return expression/condition 
		})

*/

let someValid = numbers.some(function(number) {
	return (number < 3);
})

console.log("Result from some method: ");
console.log(someValid);

/*
	filter()
	 - returns a new array that contains elemets which meets the given condition. Returns an empty array if no elements were found (that satified the given condition)

	Syntax:
	 	arrayName.filter(function(individualElement) {
			return expression/condition 
		})
*/

let filteredValid = numbers.filter(function(number) {
	return (number < 5);
})

console.log("Result of filter method: ")
console.log(filteredValid);

console.log("");

/*
	includes()
		- checks if the argument passed can be found in an array
		- can be chained after another method. The result of the first method is used on the second method until all chained methods have been resolved.

*/

let products = ["mouse", "KEYBOARD", "laptop", "monitor"]

let filteredProducts = products.filter(function(product) {
	return product.toLowerCase().includes("a")
})

console.log("result of includes methods: ")
console.log(filteredProducts);