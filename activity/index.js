/*
    Create functions which can manipulate our arrays.
    You may change the names on the initial list before you use the array varaibles.
*/

let registeredUsers = [

    "Selena Gomez",
    "Martha Hunt",
    "Emma Stone",
    "Gigi Hadid",
    "Blake Lively",
    "Hailee Steinfeld",
    "Karlie Kloss"
];


let friendsList = [];

/*
    
   1. Create a function which will allow us to register into the registeredUsers list.
        - this function should be able to receive a string.
        - determine if the input username already exists in our registeredUsers array.
            -if it is, show an alert window with the following message:
                "Registration failed. Username already exists!"
            -if it is not, add the new username into the registeredUsers array and show an alert:
                "Thank you for registering!"
        - Invoke and register a new user in the browser console.
        - In the browser console, log the registeredUsers array.

*/



    function registerNewUser (newUser) {
        let usernameAlreadyExist = registeredUsers.includes(newUser);

        if(usernameAlreadyExist) {
            alert("Registration failed. Username already exists!");
            console.log("Register: " + newUser)
        } else {
            registeredUsers.push(newUser);
            alert("Thank you for registering!");
            console.log("Register: " + newUser);
        }
    };

    registerNewUser("Taylor Swift");
    console.log("Registered Users: ")
    console.log(registeredUsers);

    registerNewUser("Selena Gomez");
    console.log("Registered Users: ")
    console.log(registeredUsers);

    console.log("")

/*
    2. Create a function which will allow us to add a registered user into our friends list.
        - This function should be able to receive a string.
        - Determine if the input username exists in our registeredUsers array.
            - If it is, add the foundUser in our friendList array.
                    -Then show an alert with the following message:
                        - "You have added <registeredUser> as a friend!"
            - If it is not, show an alert window with the following message:
                - "User not found."
        - Invoke the function and add a registered user in your friendsList in the browser console.
        - In the browser console, log the friendsList array in the console.

*/
    function AddFriend (newFriend) {
        let AddToList = registeredUsers.includes(newFriend);

        if(AddToList) {
            friendsList.push(newFriend)
            alert("You have added " + newFriend + " as a friend!");
            console.log("Added a friend: ");
            console.log(newFriend);
        } else {
            alert("User not found!")
            console.log("Added a friend: ");
            console.log(newFriend);
        }
    }

    AddFriend("Emma Stone");
    console.log("Friends List:  ")
    console.log(friendsList);

    console.log("")

    AddFriend("Selena Gomez");
    console.log("Friends List:  ")
    console.log(friendsList);

    console.log("")

    AddFriend("Blake Lively");
    console.log("Friends List:  ")
    console.log(friendsList);

    console.log("")

    AddFriend("Kendall Gener");
    console.log("Friends List:  ")
    console.log(friendsList);

    console.log("")
/*
    3. Create a function which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty show an alert: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function.

*/
    console.log("Display Friends: ");

    friendsList.forEach(function(entry) {
        console.log(entry);
    })
 
    console.log("")
    
// STRETCH GOALS

/*
    4. Create a function which will display the amount of registered users in your friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty show an alert:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function in the browser console.
*/
    
    function numberOfRegFriends (regFriends) {
        let registeredfriends = friendsList.length;

        if(registeredfriends ++) {
            alert("You currently have " + friendsList.length + " friends")
        } else {
            alert("You currently have " + friendsList.length + " friends. Add one first.")
        }
    }

    numberOfRegFriends(friendsList.length)
    console.log("Display number of friends: " + friendsList.length);

/*
    5. Create a function which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
        - In the browser console, log the friendsList array.

*/
    
    let deleteFriend = friendsList.pop();
    console.log("Deleted: " + deleteFriend);
    console.log(friendsList);

/*======================================================================================*/



// Try this for fun:
/*
   

    Instead of only deleting the last registered user in the friendsList delete a specific user instead.
        -You may get the user's index.
        -Then delete the specific user with splice().

*/
    let deleteUsingSpliceMethod = friendsList.splice(1);
    console.log("Deleted: " + deleteUsingSpliceMethod);
    console.log(friendsList);
